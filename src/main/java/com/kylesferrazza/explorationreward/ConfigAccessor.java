package com.kylesferrazza.explorationreward;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by Kyle Sferrazza on 8/8/2014.
 * This file is a part of: ExplorationReward.
 * All rights reserved.
 */
@SuppressWarnings("WeakerAccess")
public class ConfigAccessor {
    private final String fileName;
    private final JavaPlugin plugin;

    private File configFile;
    private FileConfiguration fileConfiguration;

    public ConfigAccessor(JavaPlugin plugin, String fileName) {
        if (plugin == null)
            throw new IllegalArgumentException("plugin cannot be null");
        this.plugin = plugin;
        this.fileName = fileName;
        File dataFolder = plugin.getDataFolder();
        if (dataFolder == null)
            throw new IllegalStateException();
        this.configFile = new File(plugin.getDataFolder(), fileName);
    }

    public void reloadConfig() {
        fileConfiguration = YamlConfiguration.loadConfiguration(configFile);

        // Look for defaults in the jar
        File defConfigFile = new File(plugin.getDataFolder(), fileName);
        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigFile);
        fileConfiguration.setDefaults(defConfig);
    }

    public FileConfiguration getConfig() {
        if (fileConfiguration == null) {
            this.reloadConfig();
        }
        return fileConfiguration;
    }

    public void saveConfig() {
        if (!(fileConfiguration == null || configFile == null)) {
            try {
                getConfig().save(configFile);
            } catch (IOException ex) {
                plugin.getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
            }
        }
    }

    public void saveDefaultConfig() {
        if (!configFile.exists()) {
            this.plugin.saveResource(fileName, false);
        }
    }

    public void updateConfig(String latestVersion) {
        if (!getConfig().isSet("config-version") || !getConfig().getString("config-version").equals(latestVersion)) {
            Map<String, Object> configValues = getConfig().getValues(true);
            File conf = new File(plugin.getDataFolder(), "config.yml");
            conf.delete();
            reloadConfig();
            saveDefaultConfig();
            reloadConfig();
            for (Map.Entry<String, Object> entry : configValues.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                getConfig().set(key, value);
            }
            getConfig().set("config-version", latestVersion);
            saveConfig();
        }
    }
}
