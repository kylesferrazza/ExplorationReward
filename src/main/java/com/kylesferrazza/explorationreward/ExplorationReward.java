package com.kylesferrazza.explorationreward;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkPopulateEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

/**
 * Created by Kyle Sferrazza on 8/8/2014.
 * This file is a part of: ExplorationReward.
 * All rights reserved.
 */
public class ExplorationReward extends JavaPlugin implements Listener{

    private final ConfigAccessor runEvery = new ConfigAccessor(this, "events.yml");

    @Override
    public void onEnable () {
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(this, this);
        runEvery.saveDefaultConfig();
        ConfigAccessor ca = new ConfigAccessor(this, "config.yml");
        ca.updateConfig("1.1");
    }

    public double playerDistanceToLoc(Player p, Location l) {
        double xdist = l.getX() - p.getLocation().getX();
        double zdist = l.getZ() - p.getLocation().getZ();
        return Math.sqrt(Math.pow(xdist, 2) + Math.pow(zdist, 2));
    }

    @EventHandler
    public void onDiscover(ChunkPopulateEvent chunkPopulateEvent) {
        double x = chunkPopulateEvent.getChunk().getX();
        double z = chunkPopulateEvent.getChunk().getZ();
        World w = chunkPopulateEvent.getWorld();
        Location chunkLoc = new Location(w, x, 0, z);
        Player player = (Player) getServer().getOnlinePlayers().toArray()[0];
        for (Player play : getServer().getOnlinePlayers()) {
            if (playerDistanceToLoc(play, chunkLoc) < playerDistanceToLoc(player, chunkLoc)) {
                player = play;
            }
        }
        reloadConfig();
        List<String> enabledWorlds = getConfig().getStringList("worlds-to-operate-in");
        if (!(enabledWorlds.contains(player.getWorld().getName()))) {
            return;
        }
        runEvery.reloadConfig();
        reloadConfig();
        int numOfEvents = 1;
        if (runEvery.getConfig().isSet(player.getUniqueId().toString())) {
            numOfEvents = runEvery.getConfig().getInt(player.getUniqueId().toString());
            numOfEvents++;
        }
        runEvery.getConfig().set(player.getUniqueId().toString(), numOfEvents);
        runEvery.saveConfig();
        if (!(numOfEvents >= getConfig().getInt("run-every"))) {
            return;
        }
        numOfEvents = 0;
        runEvery.getConfig().set(player.getUniqueId().toString(), numOfEvents);
        runEvery.saveConfig();
        List<String> commandsToRun = getConfig().getStringList("commands");
        Bukkit.broadcastMessage(player.getName());
        for (String command : commandsToRun) {
            command = command.replace("%p", player.getName());
            command = command.replace("%w", player.getWorld().getName());
            command = ChatColor.translateAlternateColorCodes('&', command);
            CommandSender console = getServer().getConsoleSender();
            Bukkit.dispatchCommand(console, command);
        }
    }
}
